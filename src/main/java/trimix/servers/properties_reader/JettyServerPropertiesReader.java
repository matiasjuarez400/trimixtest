package trimix.servers.properties_reader;

import org.springframework.stereotype.Component;

@Component
public class JettyServerPropertiesReader extends BasePropertiesReader {
    private static final String PROPERTIES_FILE = "jettyserver.properties";

    public JettyServerPropertiesReader() {
        super(PROPERTIES_FILE);
    }

    public Integer getServerPort() {
        return Integer.parseInt(getProperties().getProperty("server.port"));
    }

    public String getServerBasePath() {
        return getProperties().getProperty("server.base.path");
    }
}
