package trimix.exception_handling;

import org.hibernate.exception.ConstraintViolationException;

public class ConstraintViolationExceptionHandler extends BaseExceptionHandler<ConstraintViolationException> {
    public ConstraintViolationExceptionHandler(ConstraintViolationException exception) {
        super(exception);
    }

    @Override
    protected void generateResponseContent() {
        this.setMessage(exception.getCause().getMessage());
        this.setCode(1);
    }
}
