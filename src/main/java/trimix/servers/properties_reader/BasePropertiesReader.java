package trimix.servers.properties_reader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class BasePropertiesReader {
    protected String propertiesFile;
    protected Properties loadedProperties;

    public BasePropertiesReader(String propertiesFile) {
        this.propertiesFile = propertiesFile;
        this.loadedProperties = loadProperties();
    }

    private Properties loadProperties() {
        Properties properties = new Properties();

        try(final InputStream stream = this.getClass().getClassLoader().
                getResourceAsStream(propertiesFile)) {

            properties.load(stream);
            return properties;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected Properties getProperties() {
        return loadedProperties;
    }
}
