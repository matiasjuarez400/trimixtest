package trimix;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import trimix.servers.properties_reader.H2ServerPropertiesReader;

public class H2ServerPropertiesReaderTest {
    private H2ServerPropertiesReader h2ServerPropertiesReader;

    @Before
    public void setup() {
        this.h2ServerPropertiesReader = new H2ServerPropertiesReader();
    }

    @Test
    public void getServerPortFrom_expectNotNullValue() {
        String port = h2ServerPropertiesReader.getServerPort();

        Assert.assertNotNull(port);
        Assert.assertTrue("Port has value", port.length() > 0);
    }
}
