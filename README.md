# Backend ABM Personas

Este proyecto contiene el código necesario para poder levantar un backend que permita realizar operaciones CRUD sobre entidades PERSONA <br />

Se trata de un proyecto self-contained. <br />
Para generar un jar ejecutable, ejecutar el comando **mvn clean package**. <br />
El JAR generado lo podremos encontrar en la carpeta "target" con el nombre "trimix-test-1.0-SNAPSHOT.jar". <br />
Para ejecutar el JAR, escribimos en consola: **java -jar trimix-test-1.0-SNAPSHOT.jar**

Cuando el programa se comience a ejecutar, se levantara un servidor Jetty embebido que está configurado por defecto para correr en el puerto **8090**. <br />
También se levantara un servidor en el puerto **8082** que nos permitirá acceder a una consola para poder visualizar la base H2 que corre en memoria y que es utilizada para este programa. <br />
Los valores que se deben utilizar para acceder a la consola de H2 son: <br />
URL JDBC: **jdbc:h2:mem:myDb** <br/>
user: **admin** <br/>
pass: **admin** <br />

Si estas configuraciones no son satisfactorias para usted, podrá editar estos puertos en los archivos "**h2server.properties**" y "**jettyserver.properties**" <br />

Dado que es un código de ejemplo, la base de datos en memoria será populada con algunas Personas de ejemplo. <br />

En la siguiente URL se pueden observar ejemplos de llamadas a los endpoints que expuestos por el servidor: **https://documenter.getpostman.com/view/2324767/S1M3vQoh?version=latest** <br />

Ante cualquier consulta, contactarme en la siguiente dirección de correo: **matiasjuarez400@gmail.com**