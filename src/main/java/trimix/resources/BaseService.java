package trimix.resources;

import trimix.hibernate.HibernateManager;

public abstract class BaseService {
    protected HibernateManager hibernateManager;

    public BaseService(HibernateManager hibernateManager) {
        this.hibernateManager = hibernateManager;
    }
}
