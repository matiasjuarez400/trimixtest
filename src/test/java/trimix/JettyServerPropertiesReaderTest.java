package trimix;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import trimix.servers.properties_reader.JettyServerPropertiesReader;

public class JettyServerPropertiesReaderTest {
    private JettyServerPropertiesReader jettyServerPropertiesReader;

    @Before
    public void setup() {
        this.jettyServerPropertiesReader = new JettyServerPropertiesReader();
    }

    @Test
    public void getServerPortFrom_expectNotNullValue() {
        Integer port = jettyServerPropertiesReader.getServerPort();

        Assert.assertNotNull(port);
        Assert.assertTrue("Port has value", port > 0);
    }

    @Test
    public void getServerBasePath_expectNotNullValue() {
        String basePath = jettyServerPropertiesReader.getServerBasePath();

        Assert.assertTrue(basePath != null && basePath.length() > 0);
    }
}
