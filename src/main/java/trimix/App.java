package trimix;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import trimix.config.ApplicationConfiguration;
import trimix.hibernate.data_initializer.DataInitializer;
import trimix.servers.H2WebServer;
import trimix.servers.JettyServer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);

        H2WebServer h2WebServerLauncher = applicationContext.getBean(H2WebServer.class);
        h2WebServerLauncher.start();

        DataInitializer dataInitializer = applicationContext.getBean(DataInitializer.class);
        dataInitializer.initialize();

        JettyServer jettyServer = applicationContext.getBean(JettyServer.class);
        jettyServer.start();
    }
}
