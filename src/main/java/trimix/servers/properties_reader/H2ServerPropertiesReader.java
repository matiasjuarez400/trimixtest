package trimix.servers.properties_reader;

import org.springframework.stereotype.Component;

@Component
public class H2ServerPropertiesReader extends BasePropertiesReader {
    private static final String PROPERTIES_FILE = "h2server.properties";

    public H2ServerPropertiesReader() {
        super(PROPERTIES_FILE);
    }

    public String getServerPort() {
        return getProperties().getProperty("server.port");
    }
}
