package trimix.servers;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import trimix.config.WebConfig;
import trimix.servers.properties_reader.JettyServerPropertiesReader;

@Component
public class JettyServer {
    private Server server;
    private JettyServerPropertiesReader jettyServerPropertiesReader;
    private static Logger LOGGER = LoggerFactory.getLogger(JettyServer.class);

    @Autowired
    public JettyServer(JettyServerPropertiesReader jettyServerPropertiesReader) {
        this.jettyServerPropertiesReader = jettyServerPropertiesReader;
    }

    public void start() throws Exception {
        if (server != null && server.isRunning()) {
            stop();
        }

        int serverPort = jettyServerPropertiesReader.getServerPort();

        LOGGER.debug("Starting server at port {}", serverPort);
        server = new Server(serverPort);
        server.setHandler(buildServletContextHandler());

        addRuntimeShutdownHook(this);

        server.start();
        LOGGER.info("Server started properly at port {}", serverPort);
        server.join();
    }

    public void stop() {
        try {
            LOGGER.debug("Stopping server...");
            server.stop();
        } catch (Exception e) {
            LOGGER.error("Couldn't stop server. Reason [{}]", e.getMessage());
        } finally {
            server.destroy();
        }
    }

    private ServletContextHandler buildServletContextHandler() {
        ServletContextHandler contextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        contextHandler.setContextPath(jettyServerPropertiesReader.getServerBasePath());

        AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
        webApplicationContext.setConfigLocation(WebConfig.class.getPackage().getName());

        DispatcherServlet dispatcherServlet = new DispatcherServlet(webApplicationContext);
        ServletHolder servletHolder = new ServletHolder("dispatcher", dispatcherServlet);

        contextHandler.addServlet(servletHolder, "/");
        contextHandler.addEventListener(new ContextLoaderListener(webApplicationContext));

        return contextHandler;
    }

    private static void addRuntimeShutdownHook(final JettyServer jettyServer) {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                if (jettyServer.server.isStarted()) {
                    jettyServer.server.setStopAtShutdown(true);
                    jettyServer.stop();
                }
            }
        }));
    }
}
