package trimix.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import trimix.servers.properties_reader.H2ServerPropertiesReader;
import trimix.servers.properties_reader.JettyServerPropertiesReader;
import trimix.servers.H2WebServer;
import trimix.servers.JettyServer;

@Configuration
@ComponentScan(basePackages = {"trimix.exception_handling", "trimix.hibernate"})
public class ApplicationConfiguration {
    @Bean
    public H2ServerPropertiesReader h2ServerPropertiesReader() {
        return new H2ServerPropertiesReader();
    }

    @Bean
    public JettyServerPropertiesReader jettyServerPropertiesReader() {
        return new JettyServerPropertiesReader();
    }

    @Bean
    public H2WebServer h2WebServer() {
        H2WebServer h2WebServer = new H2WebServer(h2ServerPropertiesReader());
        return h2WebServer;
    }

    @Bean
    public JettyServer jettyServer() {
        JettyServer jettyServer = new JettyServer(jettyServerPropertiesReader());
        return jettyServer;
    }
}
