package trimix.hibernate.data_initializer;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import trimix.hibernate.HibernateManager;
import trimix.hibernate.HibernateTaskAdapter;
import trimix.resources.person.Persona;
import trimix.resources.person.TipoDocumento;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class DataInitializer {
    private HibernateManager hibernateManager;

    @Autowired
    public DataInitializer(HibernateManager hibernateManager) {
        this.hibernateManager = hibernateManager;
    }

    public void initialize() {
        List<Persona> personas = Arrays.asList(
                new Persona("Matias", "Juarez", buildDate(1991, 3, 12), 35000000, TipoDocumento.DNI),
                new Persona("Vladimir", "Putin", buildDate(1952, 9, 7), 18000000, TipoDocumento.PASAPORTE),
                new Persona("Ricardo", "Fort", buildDate(1968, 10, 5), 24000000, TipoDocumento.DNI),
                new Persona("Amy", "Lee", buildDate(1981, 11, 13), 30000000, TipoDocumento.CEDULA),
                new Persona("Daenerys", "Targaryen", buildDate(1986, 9, 23), 31000000, TipoDocumento.PASAPORTE)
        );

        hibernateManager.executeTask(new HibernateTaskAdapter() {
            @Override
            public void execute(Session session) {
                for (Persona person : personas) {
                    session.save(person);
                }
            }
        });
    }

    private Date buildDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DATE, day);

        return calendar.getTime();
    }
}
