package trimix.servers;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import trimix.servers.properties_reader.H2ServerPropertiesReader;

import java.sql.SQLException;

@Component
public class H2WebServer {
    private Server server;
    private H2ServerPropertiesReader h2ServerPropertiesReader;

    @Autowired
    public H2WebServer(H2ServerPropertiesReader h2ServerPropertiesReader) {
        this.h2ServerPropertiesReader = h2ServerPropertiesReader;
    }

    public void start() throws SQLException {
        server = Server.
                createWebServer("-web", "-webAllowOthers", "-webPort", h2ServerPropertiesReader.getServerPort()).
                start();

        addRuntimeShutdownHook(this);
    }

    public void stop() {
        if (server != null && server.isRunning(true)) {
            server.stop();
        }
    }

    private static void addRuntimeShutdownHook(final H2WebServer h2WebServer) {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                h2WebServer.stop();
            }
        }));
    }
}
