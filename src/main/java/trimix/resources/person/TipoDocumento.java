package trimix.resources.person;

public enum TipoDocumento {
    DNI("Dni"),
    PASAPORTE("Pasaporte"),
    CEDULA("Cédula");

    private String value;

    TipoDocumento(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
