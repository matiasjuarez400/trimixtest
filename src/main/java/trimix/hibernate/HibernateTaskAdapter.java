package trimix.hibernate;

import org.hibernate.Session;

public abstract class HibernateTaskAdapter implements HibernateTask {
    @Override
    public void execute(Session session) {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> T executeAndReturn(Session session) {
        throw new UnsupportedOperationException();
    }
}
