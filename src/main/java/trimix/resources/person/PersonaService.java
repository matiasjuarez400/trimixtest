package trimix.resources.person;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import trimix.hibernate.HibernateTaskAdapter;
import trimix.hibernate.HibernateManager;
import trimix.resources.BaseService;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class PersonaService extends BaseService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonaService.class);

    @Autowired
    public PersonaService(HibernateManager hibernateManager) {
        super(hibernateManager);
    }

    public void save(Persona person) {
        LOGGER.info("Saving Persona: [{}]", person);
        hibernateManager.executeTask(new HibernateTaskAdapter() {
            @Override
            public void execute(Session session) {
                session.save(person);
            }
        });
    }

    public void save(List<Persona> people) {
        hibernateManager.executeTask(new HibernateTaskAdapter() {
            @Override
            public void execute(Session session) {
                session.save(people);
            }
        });
    }

    public Persona retrieveById(long id) {
        LOGGER.info("Retrieving Persona with id: [{}]", id);
        Persona persona = hibernateManager.executeTaskAndReturn(new HibernateTaskAdapter() {
            @Override
            public Persona executeAndReturn(Session session) {
                return session.get(Persona.class, id);
            }
        });

        return persona;
    }

    public List<Persona> retrieveByConditions(String nombre, String apellido, TipoDocumento tipoDocumento, Long documento) {
        LOGGER.info("Retrieving Persona with conditions: [nombre - {}], [apellido - {}], [tipoDocumento - {}], [documento - {}]", nombre, apellido, tipoDocumento, documento);
        List<Persona> personas = hibernateManager.executeTaskAndReturn(new HibernateTaskAdapter() {
            @Override
            public List<Persona> executeAndReturn(Session session) {
                CriteriaBuilder cb = session.getCriteriaBuilder();
                CriteriaQuery<Persona> query = cb.createQuery(Persona.class);
                Root<Persona> from = query.from(Persona.class);
                query.select(from);

                List<Predicate> predicates = new ArrayList<>();
                if (nombre != null) {
                    predicates.add(cb.like(
                            cb.lower(
                                    from.get("nombre")
                            ), "%" + nombre.toLowerCase() + "%"));
                }
                if (apellido != null) {
                    predicates.add(cb.like(
                            cb.lower(
                                    from.get("apellido")
                            ), "%" + apellido.toLowerCase() + "%"));
                }
                if (tipoDocumento != null) {
                    predicates.add(cb.equal(from.get("tipoDocumento"), tipoDocumento));
                }
                if (documento != null) {
                    predicates.add(cb.equal(from.get("numeroDocumento"), documento));
                }

                Predicate mainPredicate = null;
                for (Predicate predicate : predicates) {
                    if (mainPredicate == null) {
                        mainPredicate = predicate;
                    } else {
                        mainPredicate = cb.and(mainPredicate, predicate);
                    }
                }

                if (mainPredicate != null) {
                    query.where(mainPredicate);
                }

                Query<Persona> personaQuery = session.createQuery(query);
                LOGGER.debug("Created criteria query: [{}]", personaQuery.getQueryString());

                return personaQuery.getResultList();
            }
        });

        return personas;
    }

    public void updatePersona(Persona newPersona) {
        hibernateManager.executeTask(new HibernateTaskAdapter() {
            @Override
            public void execute(Session session) {
                session.update(newPersona);
            }
        });
    }

    public void deletePersona(Persona persona) {
        hibernateManager.executeTask(new HibernateTaskAdapter() {
            @Override
            public void execute(Session session) {
                session.delete(persona);
            }
        });
    }
}
