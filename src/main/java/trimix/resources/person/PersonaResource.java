package trimix.resources.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/personas", produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonaResource {
    private PersonaService service;

    @Autowired
    public PersonaResource(PersonaService service) {
        this.service = service;
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Persona> getPersonaById(@PathVariable(name = "id") long id) {
        Persona person = service.retrieveById(id);

        if (person == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(person, HttpStatus.OK);
        }
    }

    @GetMapping
    @ResponseBody
    // TODO: Add maximum amount of entities limit
    public ResponseEntity<List<Persona>> getPersonasByFilter(@RequestParam(name = "nombre", required = false) String nombre,
                                                             @RequestParam(name = "apellido", required = false) String apellido,
                                                             @RequestParam(name = "tipoDocumento", required = false) TipoDocumento tipoDocumento,
                                                             @RequestParam(name = "numeroDocumento", required = false) Long numeroDocumento) {
        List<Persona> personas = service.retrieveByConditions(nombre, apellido, tipoDocumento, numeroDocumento);

        if (personas == null || personas.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(personas, HttpStatus.OK);
        }
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Persona> createPersona(@RequestBody Persona persona) {
        service.save(persona);

        return new ResponseEntity<>(persona, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Persona> updatePersona(@PathVariable Long id,
                                                 @RequestBody Persona persona) {
        Persona currentPersona = service.retrieveById(id);
        if (currentPersona != null) {
            persona.setId(id);
            service.updatePersona(persona);

            return new ResponseEntity<>(persona, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    @ResponseBody
    // TODO: modify functionality to flag entities as 'disabled' instead of deleting them!
    public ResponseEntity<Persona> deletePersona(@PathVariable Long id) {
        Persona currentPersona = service.retrieveById(id);
        if (currentPersona != null) {
            service.deletePersona(currentPersona);
            return new ResponseEntity<>(currentPersona, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public void setService(PersonaService service) {
        this.service = service;
    }
}
