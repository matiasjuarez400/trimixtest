package trimix.exception_handling;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
public class BaseExceptionHandler<T extends Exception> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseExceptionHandler.class);

    protected String type;
    protected int code;
    protected String message;
    @JsonIgnore
    protected T exception;

    public BaseExceptionHandler(T exception) {
        this.exception = exception;
        this.type = exception.getClass().getName();
        generateResponseContent();
    }

    protected void generateResponseContent() {
        this.message = exception.getMessage();
        this.code = 99999;
    }
}
