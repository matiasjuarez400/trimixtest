package trimix.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import trimix.resources.BaseService;

@ComponentScan(basePackageClasses = BaseService.class)
@EnableWebMvc
@Configuration
public class WebConfig {
}
