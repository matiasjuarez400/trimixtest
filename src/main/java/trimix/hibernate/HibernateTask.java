package trimix.hibernate;

import org.hibernate.Session;

public interface HibernateTask {
    void execute(Session session);
    <T> T executeAndReturn(Session session);
}
