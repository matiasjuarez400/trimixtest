package trimix.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class HibernateManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(HibernateManager.class);
    private static SessionFactory sessionFactory;

    public HibernateManager() {
        if (sessionFactory == null) {
            String configurationFile = "hibernate.cfg.xml";

            LOGGER.debug("Initializing SessionFactory from file {}", configurationFile);

            sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    if (sessionFactory != null && sessionFactory.isOpen()) {
                        sessionFactory.close();
                    }
                }
            }));

            LOGGER.debug("SessionFactory initialized properly!");
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void executeTask(HibernateTask hibernateTask) {
        executeTask(hibernateTask, true);
    }

    public void executeTask(HibernateTask hibernateTask, boolean useTransaction) {
        doExecute(hibernateTask, useTransaction, false);
    }

    public <T> T executeTaskAndReturn(HibernateTask hibernateTask) {
        return executeTaskAndReturn(hibernateTask, true);
    }

    public <T> T executeTaskAndReturn(HibernateTask hibernateTask, boolean useTransaction) {
        return doExecute(hibernateTask, useTransaction, true);
    }

    private <T> T doExecute(HibernateTask hibernateTask, boolean useTransaction, boolean returnValue) {
        Session session = sessionFactory.openSession();

        LOGGER.debug("Executing hibernateTask. [useTransaction, {}] - [returnValue, {}]", useTransaction, returnValue);
        try {
            if (useTransaction) session.beginTransaction();

            T result = null;
            if (returnValue) {
                result = hibernateTask.executeAndReturn(session);
            } else {
                hibernateTask.execute(session);
            }

            if (useTransaction) session.getTransaction().commit();

            return result;
        } catch (HibernateException he) {
            if (useTransaction) session.getTransaction().rollback();
            LOGGER.error("Error executing hibernateTask [{}]", he);
            throw he;
        } finally {
            session.close();
        }
    }
}
