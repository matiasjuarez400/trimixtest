package trimix;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import trimix.config.WebConfig;
import trimix.hibernate.HibernateManager;
import trimix.resources.person.Persona;
import trimix.resources.person.PersonaResource;
import trimix.resources.person.PersonaService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfig.class, HibernateManager.class})
@WebAppConfiguration
public class PersonaResourceTest {
    private MockMvc mockMvc;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PersonaResource personaResource;

    private PersonaService spyPersonaService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        spyPersonaService = Mockito.spy(personaService);
        personaResource.setService(spyPersonaService);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getPersonaWithId1_expectResultOK_entityPersonaReturnedInResponse() throws Exception {
        long id = 1;
        Persona persona = new Persona();
        persona.setId(id);
        persona.setNombre("Persona de prueba");

        Mockito.doReturn(persona).when(spyPersonaService).retrieveById(id);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/personas/1"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        Persona personaResultado = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Persona.class);

        Assert.assertEquals(persona.getId(), personaResultado.getId());
        Assert.assertEquals(persona.getNombre(), personaResultado.getNombre());

        Mockito.verify(spyPersonaService, Mockito.times(1)).retrieveById(id);
        Mockito.verifyNoMoreInteractions(spyPersonaService);
    }
}
