package trimix.exception_handling;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.hibernate.exception.ConstraintViolationException;
import javax.ws.rs.Produces;

@ControllerAdvice
public class ExceptionHandler {
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
    @ResponseBody
    @Produces(MediaType.APPLICATION_JSON_VALUE)
    public BaseExceptionHandler handlePersistenceException(Exception e) {
        BaseExceptionHandler baseExceptionHandler;

        if (e instanceof ConstraintViolationException) {
            baseExceptionHandler = new ConstraintViolationExceptionHandler((ConstraintViolationException) e);
        } else {
            baseExceptionHandler = new BaseExceptionHandler<>(e);
        }

        return baseExceptionHandler;
    }
}
